package org.kllbff.edu.fmove;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

public class TestFilesMover {
	public static final String INPUT_DIR_ORIGINAL  = "dir-in",
							   OUTPUT_DIR_ORIGINAL = "dir-out",
							   INPUT_DIR_FAIL      = "dir-in-null";
	public static final String INPUT_FILE_ORIGINAL  = "file-in.txt",
							   OUTPUT_FILE_ORIGINAL = "file-out.txt",
							   INPUT_FILE_FAIL      = "file-in";

	@Test
	public void testResultOk() {
		FilesMover mover = new FilesMover(INPUT_DIR_ORIGINAL, OUTPUT_DIR_ORIGINAL);
		assertEquals(FilesMover.SUCCESS, mover.move());
		resetDirChanges();
	}
	
	@Test
	public void testResultTargetExists() throws IOException {
		Path outputDirectory = Paths.get(OUTPUT_DIR_ORIGINAL); 
		Files.createDirectory(outputDirectory);
		
		FilesMover mover = new FilesMover(INPUT_DIR_ORIGINAL, OUTPUT_DIR_ORIGINAL);
		assertEquals(FilesMover.TARGET_EXISTS, mover.move());
		
		Files.delete(outputDirectory);
	}
	
	@Test
	public void testResultSrcDoesNotExists() {
		FilesMover mover = new FilesMover(INPUT_DIR_FAIL, OUTPUT_DIR_ORIGINAL);
		assertEquals(FilesMover.SOURCE_DOESNT_EXISTS, mover.move());
	}
	
	@Test
	public void testDirMoving() {
		FilesMover mover = new FilesMover(INPUT_DIR_ORIGINAL, OUTPUT_DIR_ORIGINAL);
		mover.move();
		assertFalse(Files.exists(Paths.get(INPUT_DIR_ORIGINAL)));
		assertTrue(Files.exists(Paths.get(OUTPUT_DIR_ORIGINAL)));
		resetDirChanges();
	}
	
	@Test
	public void testFileMoving() {
		FilesMover mover = new FilesMover(INPUT_FILE_ORIGINAL, OUTPUT_FILE_ORIGINAL);
		mover.move();
		assertFalse(Files.exists(Paths.get(INPUT_FILE_ORIGINAL)));
		assertTrue(Files.exists(Paths.get(OUTPUT_FILE_ORIGINAL)));
		resetFileChanges();
	}
	
	public void resetDirChanges() {
		FilesMover mover = new FilesMover(OUTPUT_DIR_ORIGINAL, INPUT_DIR_ORIGINAL);
		assertEquals(FilesMover.SUCCESS, mover.move());
	}
	
	public void resetFileChanges() {
		FilesMover mover = new FilesMover(OUTPUT_FILE_ORIGINAL, INPUT_FILE_ORIGINAL);
		assertEquals(FilesMover.SUCCESS, mover.move());
	}
}
