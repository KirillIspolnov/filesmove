package org.kllbff.edu.fmove;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Представляет собой простой инструмент для перемещения
 * файлов и папок (включая вложенные папки и их содержимое)
 * средствами системы ввода-вывода {@link java.nio.file}<br>
 * 
 * @see java.nio.file.Files
 * @see java.nio.file.Paths
 * @see java.nio.file.Path
 * @see java.nio.file.DirectoryStream
 *  
 * @author Kirill Ispolnov, 16it18k
 */
public class FilesMover {
	/**
	 * Исходный файл/папка не найден
	 */
	public static final int SOURCE_DOESNT_EXISTS = 0x00;
	/**
	 * Целевой файл/папка уже существует
	 */
	public static final int TARGET_EXISTS 		 = 0x02;
	/**
	 * Непредвиденная ошибка ввода-вывода
	 */
	public static final int IO_EXCEPTION 		 = 0x04;
	/**
	 * Некорректный путь
	 */
	public static final int INVALID_PATH 	     = 0x08;
	/**
	 * Успешное перемещение файла/папки
	 */
	public static final int SUCCESS				 = 0x10;
	
	private Path source;
	private Path target;
	private List<Path> movedPaths;
	
	/**
	 * Инициализирует объект используя полученные путь к исходному файлу
     * и путь к файлу копии
     * 
     * @param source путь к исходному файлу или папке-источнику
     * @param target путь к конечному файлу или папке-приемнику
	 */
	public FilesMover(String source, String target) {
		this.source = Paths.get(source);
		this.target = Paths.get(target);
		this.movedPaths = new ArrayList<Path>();
	}
	
	/**
	 * Выполняет перемещение, вовзращая одно из значений
	 * <i>{@link #SUCCESS}, {@link #SOURCE_DOESNT_EXISTS}, {@link #TARGET_EXISTS}, {@link #IO_EXCEPTION}, {@link #INVALID_PATH}</i>
	 * в качестве результата работы
	 * 
	 * @return результат перемещения, представленный одним из флагов
	 */
	public int move() {
		try {
			if(!Files.exists(source)) {
				return SOURCE_DOESNT_EXISTS;
			}
			
			if(!Files.isDirectory(source)) {
				moveFile(source, target);
			} else {
				moveDir(source, target);
			}
		} catch (FileAlreadyExistsException faee) {
			return TARGET_EXISTS;
		} catch(InvalidPathException ipe) {
			return INVALID_PATH;
		} catch(IOException ioe) {
			return IO_EXCEPTION;
		}
		return SUCCESS;
	}
	
	/**
	 * Перемещает папку со всем содержимым
	 * 
	 * @param sourceDir путь к папке-источнику
	 * @param targetDir путь к папке-приемнику
	 */
	private void moveDir(Path sourceDir, Path targetDir) throws IOException {
		//создание папки-приемника
		Files.createDirectory(targetDir);
		DirectoryStream<Path> directoryStream = Files.newDirectoryStream(sourceDir);
		//перебор содержимого папки
		for(Path sourceFile : directoryStream) {
			//путь к новому расположению этого файла
			Path targetFile = Paths.get(targetDir.toString(), sourceFile.getFileName().toString());
			if(Files.isDirectory(sourceFile)) {
				//засада, папка в папке... Что ж, придется перемещать...
				moveDir(sourceFile, targetFile);
			} else {
				//копируем файлик (не папку)
				moveFile(sourceFile, targetFile);
			}
		}
		//хлоп
		directoryStream.close();
		//мы переместили содержимое папки, она осталась пуста. Пора её удалить!
		Files.delete(sourceDir);
	}

	/**
	 * Перемещает файл и сохраняет его имя в {@link #movedPaths}
	 * 
	 * @param sourceFile путь к исходному файлу
	 * @param targetFile путь к новому расположению файла
	 */
	private void moveFile(Path sourceFile, Path targetFile) throws IOException {
		Files.move(sourceFile, targetFile);
		movedPaths.add(sourceFile.getFileName());
	}
	
	/**
	 * Возвращает список перемещённых файлов<br>
	 * Внимание! Список перемещенных файлов может отличаться от содержимого
	 * папки-источника, т.к. в зоде перемещения отдельных файлов могла
	 * возникнуть ошибка, которая привела к прерыванию процесса перемещения.
	 * В этом случае данный метод вернет список тех файлов, что были успешно перемещены 
	 * 
	 * @return список перемещённых файлов
	 */
	public List<Path> getMovedFilesList() {
		return movedPaths;
	}
}
