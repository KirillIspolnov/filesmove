package org.kllbff.edu.fmove;

import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;

public class Launcher {
	private static final String RESULT_FORMAT           = "Перемещено %d файл%s: %s";
	private static final String NO_MOVED_FILES_MSG      = "Не перемещено ниодного файла";
	private static final String TARGET_EXISTS_ERR       = "Конечный файл или папка-приемник уже существует";
	private static final String SOURCE_DOESNT_EXIST_ERR = "Исходный файл или папка-источник не существует";
	private static final String IO_EXCEPTION_ERR        = "Возникла непредвиденная ошибка ввода-вывода";
	private static final String INVALID_PATH_ERR        = "Путь введён некорректно";
	private static final String MOVED_SUCCESSFULLY_MSG  = "Перемещение успешно завершено";
	private static final String INPUT_TARGET_PATH_MSG   = "Введите путь, по которому нужно перместить файл или путь к папке-источнику: ";
	private static final String INPUT_SOURCE_PATH_MSG   = "Введите путь к исходному файлу или папке-источнику: ";
	
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.print(INPUT_SOURCE_PATH_MSG);
		String source = scanner.nextLine();
		System.out.print(INPUT_TARGET_PATH_MSG);
		String target = scanner.nextLine();
		
		FilesMover mover = new FilesMover(source, target);
		switch(mover.move()) {
			case FilesMover.SUCCESS:
				System.out.println(MOVED_SUCCESSFULLY_MSG);
				break;
			case FilesMover.INVALID_PATH:
				System.out.println(INVALID_PATH_ERR);
				break;
			case FilesMover.IO_EXCEPTION:
				System.out.println(IO_EXCEPTION_ERR);
				break;
			case FilesMover.SOURCE_DOESNT_EXISTS: 
				System.out.println(SOURCE_DOESNT_EXIST_ERR);
				break;
			case FilesMover.TARGET_EXISTS: 
				System.out.println(TARGET_EXISTS_ERR);
				break;
			default: break;
		}
		
		List<Path> files = mover.getMovedFilesList();
		int filesCount = files.size();
		if(filesCount > 0) {
			System.out.printf(RESULT_FORMAT, filesCount, validWordEndFor(filesCount), files);
		} else {
			System.out.println(NO_MOVED_FILES_MSG);
		}
	}
	
	public static String validWordEndFor(int count) {
		if(count / 10 == 1) {
			return "ов";
		}
		
		switch(count % 10) {
			case 1: return "";
			case 2: case 3: case 4: return "а";
			default: return "ов";
		}
	}

}
